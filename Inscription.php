<?php

include_once 'index.php';
$results["error"] = false;
$results["message"] = [];
$db= ConnexionPDO();
 
//echo 'salut';
 
if(isset($_POST)){
    
    
   extract($_POST);
   
   
 //  echo json_encode($_POST);
   
   if(!empty($pseudo) && !empty($email) && !empty($password) && !empty($confimation_password)){
       
       
        
       // vérification pseudo
       if(strlen($pseudo) <3 || !preg_match("/^[a-zA-Z0-9_-]+$/", $pseudo) || strlen($pseudo) > 20)
       {
            $results["error"] = true;
            $results["message"]= "Pseudo invalide"; 
       }
       else
       {
           $requete = $db->prepare("SELECT pseudo FROM users WHERE pseudo = :pseudo");
           $requete->execute([':pseudo' => $pseudo]);
           $row = $requete->fetch();
           if($row){
              $results["error"] = true;
       $results["message"] = "Un compte existe déjà avec cet pseudo"; 
           }
       }
        
        // vérification email
       if (!filter_var($email, FILTER_VALIDATE_EMAIL))
       {
            $results["error"] = true;
            $results["message"] = "Email invalide";
       }
       else
       {
           $requete = $db->prepare("SELECT email FROM users WHERE email = :email");
           $requete->execute([':email' => $email]);
           $row = $requete->fetch();
           if($row){
              $results["error"] = true;
       $results["message"] = "Un compte existe déjà avec cet email"; 
           }
       }
        
       //vérification mot de passe
       if($password !== $confimation_password){
            $results["error"] = true;
       $results["message"] = "Les mots de passe doivent être identiques";
       }
       
        
       if($results["error"] === false){
           $password = password_hash($password, PASSWORD_BCRYPT);
           $sql = $db->prepare("INSERT INTO users(pseudo, email, password) VALUES (:pseudo, :email, :password)");
           $sql->execute([":pseudo" => $pseudo, ":email" => $email, ":password" => $password]);
           $results["message"] ="Inscription Reussite";
           $path ="C:/xampp/htdocs/AndroidTP/user_files/". $pseudo;
           mkdir($path,true);
           if(!$sql){
                $results["error"] = true;
       $results["message"] = "Erreur lors de l'inscription";
           }
       }
   }else{
       $results["error"] = true;
       $results["message"] = "Veuillez remplir tous les champs";
   }
     
    echo json_encode($results);
    
   
}

?>






































