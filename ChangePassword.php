<?php
include 'index.php';
$db=ConnexionPDO();
if(isset($_POST)){

    $result['message']="";
    extract($_POST);
    if(!empty($pseudo) && !empty($password_init) && !empty($password_new)){

        echo " ".$pseudo."  ".$password_init."    ".$password_new;
        $sql = $db->prepare("SELECT * FROM users WHERE pseudo = :pseudo ");
        $sql->execute([":pseudo" => $pseudo]);
        $row = $sql->fetch(PDO::FETCH_ASSOC);
        if($row){
            $password = password_hash($password_new, PASSWORD_BCRYPT);
        	$sql = $db->prepare("UPDATE users SET password =:password WHERE pseudo = :pseudo ");
        	$sql->execute([":password" => $password,
        					":pseudo" => $pseudo]);

                $result["message"]="success";
                $result["password"]=$row->password;
        
                
            }
            $result["message"]="failed";

        }
        
        
        echo (json_encode($result));
        
    }
    ?>